package main

import (
	"strconv"
)

// Day11 Run Part 1 of Day 1
func Day11() string {
	var sum int64

	for _, element := range Input {
		val, _ := strconv.ParseInt(element, 10, 64)
		sum += val
	}

	return strconv.FormatInt(sum, 10)
}

// Day12 Run Day 1 Part 2
func Day12() string {
	var sum int64
	var found bool

	reached := make(map[int64]bool)

	for !found {
		for _, element := range Input {
			val, _ := strconv.ParseInt(element, 10, 64)
			sum += val

			_, exists := reached[sum]

			if exists {
				found = true
				break
			}

			reached[sum] = true
		}
	}

	return strconv.FormatInt(sum, 10)
}
