package day04

import "sort"

type actionSorter struct {
	actions []Action
	by      func(a1, a2 *Action) bool
}

// By is the type of a "less" funtion taht defines the ordering of its Action arguments
type By func(a1, a2 *Action) bool

// Sort is a method on the function type, By, that sorts the argumentslice according to the function
func (by By) Sort(actions []Action) {
	as := &actionSorter{
		actions: actions,
		by:      by,
	}
	sort.Sort(as)
}

func (a *actionSorter) Len() int {
	return len(a.actions)
}

func (a *actionSorter) Swap(i, j int) {
	a.actions[i], a.actions[j] = a.actions[j], a.actions[i]
}

func (a *actionSorter) Less(i, j int) bool {
	return a.by(&a.actions[i], &a.actions[j])
}
