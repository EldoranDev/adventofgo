package day04

func GetSum(hour []int) int {
	sum := 0

	for i := 0; i < 60; i++ {
		sum += hour[i]
	}

	return sum
}

func GetHighest(hour []int) int {
	highest := -1
	highestIndex := -1

	for i := 0; i < 60; i++ {
		if highest < hour[i] {
			highest = hour[i]
			highestIndex = i
		}
	}

	return highestIndex
}
