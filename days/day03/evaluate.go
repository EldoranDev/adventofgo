package day03

import (
	"regexp"
	"strconv"
)

// Pattern regexp for claims
const Pattern = `\#([0-9]*)\s\@\s([0-9]*),([0-9]*)\:\s([0-9]*)x([0-9]*)`

// EvaluateSclice evaluate the claims for given input
func EvaluateSclice(vals []string, size int, result chan<- *[][]int) {
	var fabric = make([][]int, size)

	for i := 0; i < size; i++ {
		fabric[i] = make([]int, size)
	}

	re := regexp.MustCompile(Pattern)

	for i := 0; i < len(vals); i++ {
		matches := re.FindStringSubmatch(vals[i])
		x, _ := strconv.ParseInt(matches[2], 10, 32)
		y, _ := strconv.ParseInt(matches[3], 10, 32)
		w, _ := strconv.ParseInt(matches[4], 10, 32)
		h, _ := strconv.ParseInt(matches[5], 10, 32)

		for xx := x; xx < x+w; xx++ {
			for yy := y; yy < y+h; yy++ {
				fabric[xx][yy]++
			}
		}
	}

	result <- &fabric
}
