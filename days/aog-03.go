package main

import (
	"fmt"
	"regexp"
	"strconv"

	"github.com/EldoranDev/AdventOfGo/days/day03"
)

// Day31 Execute Day 3 Part 1
func Day31() string {
	var goCount = 1
	const SIZE = 1000

	var result int

	results := make(chan *[][]int)

	sliceLength := InputLength / goCount

	for i := 0; i < goCount; i++ {
		go day03.EvaluateSclice(Input[i*sliceLength:i*sliceLength+sliceLength], SIZE, results)
	}

	if sliceLength*goCount < InputLength {
		go day03.EvaluateSclice(Input[sliceLength*goCount:InputLength], SIZE, results)
		goCount++
	}

	sclices := make([][][]int, goCount)

	for i := 0; i < goCount; i++ {
		sclices[i] = *(<-results)
	}
	close(results)

	for x := 0; x < SIZE; x++ {
		for y := 0; y < SIZE; y++ {
			var sum int
			for i := 0; i < goCount; i++ {
				sum += sclices[i][x][y]
			}

			if sum > 1 {
				result++
			}
		}
	}

	return fmt.Sprintf("%v", result)
}

// Day32 Execute Day 3 Part 2
func Day32() string {
	const SIZE = 1000
	claims := make(map[int64]bool)

	fabric := make([][]int64, SIZE)

	for i := 0; i < SIZE; i++ {
		fabric[i] = make([]int64, SIZE)
	}

	rg := regexp.MustCompile(day03.Pattern)

	for i := 0; i < InputLength; i++ {
		matches := rg.FindStringSubmatch(Input[i])

		match, _ := strconv.ParseInt(matches[1], 10, 32)
		x, _ := strconv.ParseInt(matches[2], 10, 32)
		y, _ := strconv.ParseInt(matches[3], 10, 32)
		w, _ := strconv.ParseInt(matches[4], 10, 32)
		h, _ := strconv.ParseInt(matches[5], 10, 32)

		claims[match] = true

		for xx := x; xx < x+w; xx++ {
			for yy := y; yy < y+h; yy++ {
				if fabric[xx][yy] != 0 {
					claims[fabric[xx][yy]] = false
					claims[match] = false
				}

				fabric[xx][yy] = match
			}
		}
	}

	for key := range claims {
		if claims[key] {
			return fmt.Sprintf("%v", key)
		}
	}

	return ""
}
