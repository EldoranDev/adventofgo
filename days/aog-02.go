package main

import (
	"strconv"
)

// Day21 Run Day 2 part 1
func Day21() string {
	var twos int64
	var thress int64

	for _, element := range Input {
		chars := make(map[rune]int64)

		for _, char := range element {
			_, has := chars[char]

			if !has {
				chars[char] = 1
			} else {
				chars[char]++
			}
		}

		var hadTwo bool
		var hadThree bool

		for _, count := range chars {
			if count == 2 && !hadTwo {
				twos++
				hadTwo = true
			}

			if count == 3 && !hadThree {
				thress++
				hadThree = true
			}
		}
	}

	return strconv.FormatInt(twos*thress, 10)
}

func Day22() string {
	var results []string

	for _, A := range Input {
		elements := len(A)

		for _, B := range Input {
			var diff int
			var miss bool

			for i := 0; i < elements && !miss; i++ {
				if A[i] != B[i] {
					diff++

					if diff > 2 {
						miss = true
					}
				}
			}

			if !miss {
				results = make([]string, 2)

				results[0] = A
				results[1] = B
				break
			}
		}

		if results != nil {
			break
		}
	}

	result := ""

	for i := 0; i < len(results[0]); i++ {
		if results[0][i] == results[1][i] {
			result += string(results[0][i])
		}
	}

	return result
}
