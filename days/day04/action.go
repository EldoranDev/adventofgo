package day04

import (
	"strconv"
	"strings"
	"time"
)

// Expression regexp that is used for the input
const Expression = `\[(.*)\]\s[a-zA-Z]*\s(\#([0-9]*))?`

// Move type of the Acton
type Move int

// FallAsleep Guards falls asleep
// WakeUp Guard wakes up
const (
	FallAsleep  Move = 0
	WakeUp      Move = 1
	BeginsShift Move = 2
)

// Action action in the list
type Action struct {
	Guard int
	Time  time.Time
	Move  Move
}

// CreateActionsFromMatches Build Action based on RegExp
func CreateActionsFromMatches(matches []string) Action {
	t, _ := time.Parse(`2006-01-02 15:04`, matches[1])

	if matches[3] == "" {
		if strings.Contains(matches[0], "falls") {
			return Action{0, t, FallAsleep}
		}

		return Action{0, t, WakeUp}
	}

	guard, _ := strconv.ParseInt(matches[3], 10, 64)

	return Action{int(guard), t, BeginsShift}
}

// Sort the the given list of Actions
func Sort(actions []Action) {
	name := func(a1, a2 *Action) bool {
		return a1.Time.Unix() < a2.Time.Unix()
	}

	By(name).Sort(actions)
}
