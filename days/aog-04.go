package main

import (
	"fmt"
	"regexp"
	"time"

	"github.com/EldoranDev/AdventOfGo/days/day04"
)

// Day41 Run day 4 part 1
func Day41() string {
	actions := make([]day04.Action, 0)
	reg := regexp.MustCompile(day04.Expression)

	for i := 0; i < InputLength; i++ {
		matches := reg.FindStringSubmatch(Input[i])
		action := day04.CreateActionsFromMatches(matches)
		actions = append(actions, action)
	}

	type guard int

	day04.Sort(actions)

	guards := make(map[int][]int)

	var guardOnDuty int
	var sleepTime time.Time

	for _, action := range actions {
		if action.Move == day04.BeginsShift {
			_, has := guards[action.Guard]

			if !has {
				guards[action.Guard] = make([]int, 60)
			}

			guardOnDuty = action.Guard
		}

		if action.Move == day04.FallAsleep {
			sleepTime = action.Time
		}

		if action.Move == day04.WakeUp {
			for i := sleepTime.Minute(); i < action.Time.Minute(); i++ {
				guards[guardOnDuty][i]++
			}
		}
	}

	var max int
	var maxGuard int

	for i := range guards {
		sleepTime := day04.GetSum(guards[i])

		if sleepTime > max {
			max = sleepTime
			maxGuard = i
		}
	}

	return fmt.Sprintf("%v", maxGuard*day04.GetHighest(guards[maxGuard]))
}

// Day42 Run day 4 part 2
func Day42() string {
	return ""
}
