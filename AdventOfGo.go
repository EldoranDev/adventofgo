package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"plugin"
	"strconv"
	"strings"
	"time"
)

func main() {
	var file string

	argCount := len(os.Args)

	if argCount < 3 {
		fmt.Println("Usage: AdventOfGo <Day> <Part>.")
		os.Exit(1)
	}

	day, errDay := strconv.ParseUint(os.Args[1], 10, 8)
	part, errPart := strconv.ParseUint(os.Args[2], 10, 8)

	if errDay != nil || errPart != nil {
		fmt.Println(errDay)
		fmt.Println(errPart)

		os.Exit(1)
	}

	if argCount > 3 {
		file = os.Args[3]
	} else {
		file = fmt.Sprintf("./input/%02d.in", day)
	}

	fmt.Printf("Will try to load Day %v Part %v.\n", day, part)

	lib, err := plugin.Open("days.so")

	if err != nil {
		panic(err)
	}

	runner, err := lib.Lookup("Day" + strconv.FormatUint(day, 10) + strconv.FormatUint(part, 10))

	if err != nil {
		panic(err)
	}

	runnerInput, err := lib.Lookup("Input")

	if err != nil {
		panic(err)
	}

	runnerInputLength, err := lib.Lookup("InputLength")

	if err != nil {
		panic(err)
	}

	data, err := ioutil.ReadFile(file)

	if err != nil {
		panic(err)
	}

	*runnerInput.(*[]string) = strings.Split(string(data), "\n")
	*runnerInputLength.(*int) = len(*runnerInput.(*[]string))

	timeStart := time.Now()

	result := runner.(func() string)()

	elapsed := time.Since(timeStart)

	fmt.Printf("Result: %v\n", result)
	fmt.Printf("Calculation took: %v\n", elapsed)
}
